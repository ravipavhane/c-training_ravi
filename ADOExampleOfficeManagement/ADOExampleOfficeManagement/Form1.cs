﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADOExampleOfficeManagement
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection("Data Source=TRI-NB0554;Initial Catalog=OfficeManagmentDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("select * from Employee", conn);

            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                MessageBox.Show("Employee Info :" + sdr[0] + sdr["Name"] + sdr.GetDecimal(2) + sdr.GetString(3));
            }

            conn.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection("Data Source=TRI-NB0554;Initial Catalog=OfficeManagmentDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("select * from Employee", conn);

            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            //DataTable dt = new DataTable();
            //dt.Load(sdr);
            //dataGridView1.DataSource = dt;
            
            BindingSource bs = new BindingSource();
            bs.DataSource = sdr;
            dataGridView1.DataSource = bs;

            conn.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection("Data Source=TRI-NB0554;Initial Catalog=OfficeManagmentDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("select count(*) from Employee", conn);

            conn.Open();
            int count =Convert.ToInt32( cmd.ExecuteScalar());
            MessageBox.Show("Employee Count = "+ count);


            conn.Close();
        }
    }
}
